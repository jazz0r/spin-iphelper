﻿; Jeff Zemla <jzemla@spielo-int.com>
Ver=1.3

RunAsAdmin()
#NoTrayIcon
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
;~ #Warn  ; Recommended for catching common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
InstallDir=C:\SPIN_tools
#SingleInstance, Force

RunWait %ComSpec% /C Ping -n 1 -w 500 bsw2kp93,, Hide
if NOT ErrorLevel {
	;~ MsgBox on network
	Gosub, Install
	Gosub, CheckVersion
}

TempFile=%A_Temp%\ip_temp.txt

Gui, Add, Groupbox, w100 h100,IP Address:
Gui, Add, Text, vIPList x20 yp+15 r4 w85
Gui, Add, Button, gGetIP w50, Refresh
Gui, Add, Button, gIPInfo w30 yp x+2, Info

;~ Gui, Add, Button, gNetSupport +Section y+5 xs, NetSupport
;~ Gui, Add, Checkbox, vProxyState ys+5, Proxy
;~ Gui, Add, Edit, vProxyServ yp x+2 w80 +Disabled

Gui, Add, Button, gReleaseIP +Section y0, Release IP
Gui, Add, Button, gRenewIP y+0 wp xs, Renew IP
Gui, Add, Button, gFlushDNS y+0 wp xs, Flush DNS
Gui, Add, Button, gGetExternalIP y+0 wp xs, External IP
Gui, Add, Button, gNetSupport y+2 xs, RemoteHelp
Gui, Font, s7, Tahoma
Gui, Add, Checkbox, vProxyState gChangeProxy x5 yp+22 w200 h20, Proxy
Gui, Font

; width: 221     height: 168
Gui, Show, w200 h135,IPHelper %Ver%
GoSub, GetIP
Return


GetProxy:
SplashTextOn, , 20,Working, Getting Proxy Info
RegRead, Proxy, HKCU, SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings, AutoConfigURL
if Proxy {
	;~ RegRead, ProxyServer, HKCU, SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings, ProxyServer
	;~ GuiControl, Enable, ProxyState
	;~ GuiControl, Enable, ProxyServ
	;~ GuiControl,, ProxyServ, %ProxyServer%
	GuiControl,, ProxyState, 1
	GuiControl, Text, ProxyState, %Proxy%
} else {
	GuiControl,, ProxyState, 0
	GuiControl, Text, ProxyState, Proxy
}
SplashTextOff
Return

ChangeProxy:
; AutoConfig ON:  46000000030000000D00000000000000000000001F000000687474703A2F2F6174617765622F50726F78792F70726F787955532E70616304000000000000008CC37EC16837CD010000000000000000000000000100000002000000AC11B132000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;AutoConfig OFF: 4600000002000000010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
GuiControlGet, ProxyState
If ProxyState {
	;~ RegWrite, REG_DWORD, HKCU, SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings, ProxyEnable, 1
	;~ RegWrite, REG_SZ, HKCU, SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings, ProxyServer, usstmg01.spin.local:3128
	;~ RegWrite, REG_SZ, HKCU, SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings, ProxyOverride, *.spin.local
	RegWrite, REG_DWORD, HKCU, SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings, ProxyEnable, 0
	RegWrite, REG_SZ, HKCU, SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings, AutoConfigURL, http://ataweb/Proxy/proxyUS.pac
	Regwrite, REG_BINARY, HKEY_CURRENT_USER, Software\Microsoft\Windows\CurrentVersion\Internet Settings\Connections,DefaultConnectionSettings,46000000030000000D00000000000000000000001F000000687474703A2F2F6174617765622F50726F78792F70726F787955532E70616304000000000000008CC37EC16837CD010000000000000000000000000100000002000000AC11B132000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
	GuiControl, Text, ProxyState, http://ataweb/Proxy/proxyUS.pac
} else {
	RegWrite, REG_DWORD, HKCU, SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings, ProxyEnable, 0
	RegDelete, HKCU, SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings, AutoConfigURL
	Regwrite, REG_BINARY, HKEY_CURRENT_USER, Software\Microsoft\Windows\CurrentVersion\Internet Settings\Connections,DefaultConnectionSettings,4600000002000000010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
	GuiControl, Text, ProxyState, Proxy
}
Return

NetSupport:
;~ Run, http://www.netviewer.com/en/joinsession/
IfInString, IPList, 172.17.
{
	GuiControlGet, ProxyState
	If not ProxyState
	{
		MsgBox, 51, IPHelper %Ver%, IPHelper has detected that you are connected to the corporate network (VPN`, hardwire`, etc.) and the proxy setting is not enabled.  Enable proxy?
		IfMsgBox Yes
		{
			GuiControl,,ProxyState, 1
			Gosub, ChangeProxy
		}
	}
}
Run, https://remotehelp.gtech.com
Return

RenewIP:
SplashTextOn, , 20,Working, Renewing IP
RunWait %comspec% /c "ipconfig /renew > %TempFile%",,Hide
SplashTextOff
GoSub, GetIP
GoSub, GetDetails
Return

ReleaseIP:
SplashTextOn, , 20,Working, Releasing IP
RunWait %comspec% /c "ipconfig /release > %TempFile%",,Hide
SplashTextOff
GoSub, GetIP
GoSub, GetDetails
Return

FlushDNS:
SplashTextOn, , 20,Working, Flushing DNS
RunWait %comspec% /c "ipconfig /flushdns > %TempFile%",,Hide
SplashTextOff
GoSub, GetDetails
Return

GetExternalIP:
SplashTextOn, , 20,Working, Getting External IP
URLDownloadToFile, http://automation.whatismyip.com/n09230945.asp, %TempFile%
SplashTextOff
FileRead, detail, %A_Temp%\ip_temp.txt
If (StrLen(detail) > 15) ;Yeah, I still need to learn RegEx.
{
   FileDelete, %TempFile%
   FileAppend, ERROR, %TempFile%
}
GoSub, GetDetails
Return

IPInfo:
SplashTextOn, , 20,Working, Getting IP Info
RunWait %comspec% /c "ipconfig.exe > %TempFile%",,Hide
SplashTextOff
GoSub, GetDetails
Return

GetIP:
SplashTextOn, , 20,Working, Getting IP List
IPList=
(
%A_IPAddress1%
%A_IPAddress2%
%A_IPAddress3%
%A_IPAddress4%
)
GuiControl, , IPList,%IPList%
SplashTextOff
GoSub, GetProxy
Return

GetDetails:
FileRead, detail, %A_Temp%\ip_temp.txt
FileDelete, %A_Temp%\ip_temp.txt
StringReplace, detail, detail, `r`n`r`n,`r`n, all
EnvGet PCName, COMPUTERNAME
detail:= "IP details for " . PCName . ":`r`n" . detail
Msgbox, ,Details, %detail%
Return

Install:
If A_ScriptDir <> %InstallDir%
{
	SetTimer, ChangeInstallButtonNames, 50
	MsgBox, 36, IPHelper Installer, It is recommended that IPHelper be installed to %InstallDir%.`nProceed with installation or run from current location?
	IfMsgBox, YES 
	{
		;run, explorer %a_temp% ;DEBUG
		FileAppend,
		(
		@echo off
		taskkill /f /im IPHelper.exe
		xcopy /y \\BSW2KP93\PROD\Public\IT\IPHelper\IPHelper.exe %InstallDir%\
		start %InstallDir%\IPHelper.exe
		del %A_Temp%\iphelper_install.bat
		), %A_Temp%\iphelper_install.bat
		MsgBox, 36, IPHelper Installer, Create desktop shortcut?
		IfMsgBox, YES
			FileCreateShortcut, %InstallDir%\%A_ScriptName%, %A_Desktop%\IPHelper.lnk, %InstallDir%
		Run, %A_Temp%\iphelper_install.bat
		ExitApp
	}
	;~ else 
		;~ MsgBox, You chose Run.
}
Return

ChangeInstallButtonNames: 
IfWinNotExist, IPHelper Installer
    return  ; Keep waiting.
SetTimer, ChangeInstallButtonNames, off 
WinActivate 
ControlSetText, Button1, &Install
ControlSetText, Button2, &Run
Return

CheckVersion:
IfExist, \\BSW2KP93\PROD\Public\IT\IPHelper\ver
	FileRead, CurrentVersion, \\BSW2KP93\PROD\Public\IT\IPHelper\ver
else ;not on network?  no ver file?  forget the update and run the program...
	return
IfExist, \\BSW2KP93\PROD\Public\IT\IPHelper\changes
	FileRead, UpdateChanges, \\BSW2KP93\PROD\Public\IT\IPHelper\changes
else
	UpdateChanges = ERROR: changes not found
IfGreater, CurrentVersion, %Ver% 
{
	;run, explorer %a_temp% ;DEBUG
	MsgBox, 68, IPHelper Updater, Update available: %CurrentVersion%`nFixes:`n%UpdateChanges%`n`nUpdate now?
	IfMsgBox No
		return
	else 
	{
		IfNotExist, \\BSW2KP93\PROD\Public\IT\IPHelper\IPHelper.exe 
		{
			MsgBox, 16, IPHelper Updater, Error: EXE update not found.  Update cancelled.
			return
		}
		FileAppend,
		(
		@echo off
		taskkill /f /im IPHelper.exe
		xcopy /y \\BSW2KP93\PROD\Public\IT\IPHelper\IPHelper.exe %A_ScriptDir%\
		start %A_ScriptDir%\IPHelper.exe
		del %A_Temp%\ip_updater.bat
		), %A_Temp%\ip_updater.bat
		Run, %A_Temp%\ip_updater.bat
		ExitApp
	}
}
;~ msgbox C: %CurrentVersion%`nV: %Ver% ;DEBUG
Return

GuiClose:
ExitApp
